<?php

namespace GetNoticed\Utils;

class FileUtils
{
    public static function findName(string $name, string $directory = '.')
    {
        $findCmd = sprintf('find %s -type f -name %s', $directory, $name);
        exec($findCmd, $output, $exitCode);

        return $output ?: [];
    }

    public static function findWholename(string $wholeName, string $directory = '.')
    {
        $findCmd = sprintf('find %s -type f -wholename %s', $directory, $wholeName);
        exec($findCmd, $output, $exitCode);

        return $output ?: [];
    }
}