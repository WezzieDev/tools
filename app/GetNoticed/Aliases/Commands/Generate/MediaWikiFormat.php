<?php

namespace GetNoticed\Aliases\Commands\Generate;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MediaWikiFormat extends Command
{

    const ALIASES_FILE_REGEX = '#^(alias\ )([a-zA-Z0-9\-\,\.]+)(\=)(.*)$#m';
    const ALIASES_FILE_PATH = ['/home/wvestjens/.bash_aliases', '/home/wvestjens/.zsh_aliases'];

    protected function configure()
    {
        $this
            ->setName('aliases:generate:mediawiki')
            ->setDescription('Convert aliases file to an aliases file.')
            ->setHelp('Generates a MediaWiki table from an aliases file.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach ($this->getAliasesData() as $record) {
            list($fullCommand, $alias, $command, $mwCode) = $record;

            $output->writeln($mwCode);
        }
    }

    protected function getAliasesData()
    {
        $aliases = [];

        foreach (self::ALIASES_FILE_PATH as $aliasesFilePath) {
            if (file_exists($aliasesFilePath)) {
                // Match aliases
                preg_match_all(self::ALIASES_FILE_REGEX, file_get_contents($aliasesFilePath), $matches);

                // Process aliases
                for ($i = 0; $i < count($matches[0]) - 1; $i++) {
                    // Commands
                    $fullLine = $matches[0][$i];
                    $aliasCommand = $matches[2][$i];
                    $actualCommand = $matches[4][$i];

                    // Trim quotes if on both sides
                    if (
                        substr($actualCommand, -1, 1) == "'"
                        && substr($actualCommand, 0, 1) == "'"
                    ) {
                        $actualCommand = trim($actualCommand, "'");
                    }

                    // Trim quotes if on both sides
                    if (
                        substr($actualCommand, -1, 1) == '"'
                        && substr($actualCommand, 0, 1) == '"'
                    ) {
                        $actualCommand = trim($actualCommand, '"');
                    }

                    // MediaWiki code
                    $mediaWikiCode = "|-" . PHP_EOL .
                        sprintf("| <nowiki>%s</nowiki>", $aliasCommand) . PHP_EOL .
                        sprintf("| <nowiki>%s</nowiki>", $actualCommand);

                    // Add to list
                    $aliases[] = [$fullLine, $aliasCommand, $actualCommand, $mediaWikiCode];
                }

                // Let's not repeat ourselves
                break;
            }
        }

        return $aliases;
    }

}