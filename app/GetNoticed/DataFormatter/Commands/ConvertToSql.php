<?php

namespace GetNoticed\DataFormatter\Commands;

use GetNoticed\DataFormatter as DF;
use Symfony\Component\Console;

class ConvertToSql
    extends Console\Command\Command
{

    const ARG_INPUT_FILE = 'input-file';
    const ARG_OUTPUT = 'output';

    const MIME_TYPES = [
        'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    ];

    const OPT_TABLE = 'table';
    const OPT_TABLE_SHORTKEY = 't';
    const OPT_FIELDS = 'fields';
    const OPT_FIELDS_SHORTKEY = 'f';
    const OPT_DEFAULTS = 'defaults';
    const OPT_DEFAULTS_SHORTKEY = 'd';
    const OPT_OUTPUT_FILE = 'output-file';

    /**
     * @var Console\Input\InputInterface
     */
    protected $consoleInput;

    /**
     * @var Console\Output\OutputInterface
     */
    protected $consoleOutput;

    /**
     * @var string
     */
    private $inputFilePath;

    /**
     * @var string
     */
    private $inputFileMimeType;

    /**
     * @var resource
     */
    private $fileInfoResource;

    protected function execute(
        Console\Input\InputInterface $input,
        Console\Output\OutputInterface $output
    ) {
        $this->consoleInput = $input;
        $this->consoleOutput = $output;
        $this->prepareEnvironment();

        if (in_array($this->getFileMimeType(), self::MIME_TYPES) !== true) {
            throw new \InvalidArgumentException('Input file is not supported');
        }

        switch (array_search($this->getFileMimeType(), self::MIME_TYPES)) {
            case 'xlsx':
                $loader = new DF\Loader\Xlsx();
                break;

            default:
                throw new \InvalidArgumentException('Failed to detect file type, please check configuration');
                break;
        }

        $result = $loader
            ->setFilePath($this->getFilePath(), $this->getFileMimeType())
            ->setSqlParameters(
                $this->consoleInput->getOption(self::OPT_TABLE),
                \json_decode($this->consoleInput->getOption(self::OPT_FIELDS), true),
                \json_decode($this->consoleInput->getOption(self::OPT_DEFAULTS), true)
            );

        switch ($this->getOutputMethod()) {
            case '-':
                $this->consoleOutput->writeln($result->getSqlInserts());
                break;

            case 'file':
                file_put_contents($this->getOutputFile(), implode(PHP_EOL, $result->getSqlInserts()));
                $output->writeln(sprintf('<info>Written results to %s</info>', $this->getOutputMethod()));
                break;

            default:
                new \InvalidArgumentException('Invalid output method');
                break;
        }
    }

    protected function configure()
    {
        $this
            ->setName('data-formatter:to-sql')
            ->setDescription('Convert a source file to an SQL query')
            ->setHelp('Convert a source file to an SQL query');

        $this->setDefinition(
            new Console\Input\InputDefinition(
                [
                    new Console\Input\InputArgument(
                        self::ARG_INPUT_FILE,
                        Console\Input\InputArgument::REQUIRED,
                        'Specify a file to use as input'
                    ),
                    new Console\Input\InputArgument(
                        self::ARG_OUTPUT,
                        Console\Input\InputArgument::OPTIONAL,
                        'Specify output (default: console)',
                        '-'
                    ),
                    new Console\Input\InputArgument(
                        self::OPT_OUTPUT_FILE,
                        Console\Input\InputArgument::OPTIONAL,
                        'Specify output argument (e.g., if output file is chosen, specify a path)'
                    ),
                    new Console\Input\InputOption(
                        self::OPT_TABLE,
                        self::OPT_TABLE_SHORTKEY,
                        Console\Input\InputOption::VALUE_REQUIRED,
                        'Which table to perform operation on?'
                    ),
                    new Console\Input\InputOption(
                        self::OPT_FIELDS,
                        self::OPT_FIELDS_SHORTKEY,
                        Console\Input\InputOption::VALUE_REQUIRED,
                        'Which fields are present? (Comma-separated)'
                    ),
                    new Console\Input\InputOption(
                        self::OPT_DEFAULTS,
                        self::OPT_DEFAULTS_SHORTKEY,
                        Console\Input\InputOption::VALUE_OPTIONAL,
                        'Defaults (key-value, formatted in JSON)',
                        '[]'
                    )
                ]
            )
        );
    }

    /**
     * @return string
     */
    protected function getFileMimeType(): string
    {
        return $this->inputFileMimeType;
    }

    /**
     * @return string
     */
    protected function getFilePath(): string
    {
        return $this->inputFilePath;
    }

    protected function prepareEnvironment(): void
    {
        $this->fileInfoResource = finfo_open(FILEINFO_MIME_TYPE);
        $this->inputFilePath = $this->consoleInput->getArgument(self::ARG_INPUT_FILE);
        $this->inputFileMimeType = finfo_file($this->fileInfoResource, $this->getFilePath());

        if (is_file($this->inputFilePath) !== true) {
            throw new \InvalidArgumentException(sprintf('File does not exist: %1', $this->inputFilePath));
        }
    }

    /**
     * @return string
     */
    protected function getOutputMethod(): string
    {
        return $this->consoleInput->getArgument(self::ARG_OUTPUT);
    }

    protected function getOutputFile(): string
    {
        $outputFile = $this->consoleInput->getArgument(self::OPT_OUTPUT_FILE);

        if ($outputFile === null || strlen($outputFile) < 1) {
            throw new \InvalidArgumentException('No output file given.');
        }

        return $outputFile;
    }

}