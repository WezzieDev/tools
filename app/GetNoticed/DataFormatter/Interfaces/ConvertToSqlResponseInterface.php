<?php

namespace GetNoticed\DataFormatter\Interfaces;

interface ConvertToSqlResponseInterface
{

    /**
     * @param string $filePath
     * @param string $mimeType
     *
     * @return \GetNoticed\DataFormatter\Interfaces\ConvertToSqlResponseInterface
     */
    public function setFilePath(string $filePath, string $mimeType): ConvertToSqlResponseInterface;

    /**
     * @param string $tableName
     * @param array  $fieldNames
     * @param array  $defaults
     *
     * @return \GetNoticed\DataFormatter\Interfaces\ConvertToSqlResponseInterface
     */
    public function setSqlParameters(
        string $tableName,
        array $fieldNames,
        array $defaults = []
    ): ConvertToSqlResponseInterface;

    /**
     * @return string[]
     */
    public function getSqlInserts(): array;

}