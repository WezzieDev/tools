<?php

namespace GetNoticed\DataFormatter\Loader;

use GetNoticed\DataFormatter as DF;
use GetNoticed\DataFormatter\Interfaces\ConvertToSqlResponseInterface;
use PhpOffice\PhpSpreadsheet as PS;

class Xlsx
    implements DF\Interfaces\ConvertToSqlResponseInterface
{

    // FILE

    /**
     * @var string
     */
    protected $filePath;

    /**
     * @var
     */
    protected $fileMimeType;

    // SQL

    /**
     * @var string
     */
    protected $tableName;

    /**
     * @var array
     */
    protected $fieldNames;

    /**
     * @var array array
     */
    protected $defaults = [];

    /**
     * @var array
     */
    protected $sqlInserts = [];

    // Misc

    /**
     * @var \PhpOffice\PhpSpreadsheet\Spreadsheet
     */
    protected $spreadsheet;

    public function setFilePath(string $filePath, string $mimeType): DF\Interfaces\ConvertToSqlResponseInterface
    {
        $this->filePath = $filePath;
        $this->fileMimeType = $mimeType;

        return $this;
    }

    public function setSqlParameters(
        string $tableName,
        array $fieldNames,
        array $defaults = []
    ): ConvertToSqlResponseInterface {
        $this->tableName = $tableName;
        $this->fieldNames = $fieldNames;
        $this->defaults = $defaults;

        return $this;
    }

    /**
     * @return array
     */
    public function getSqlInserts(): array
    {
        try {
            $records = $this->getRecords();
            $sqlInserts = [];

            foreach ($records as $record) {
                $sqlInserts[] = sprintf(
                    'INSERT INTO %1$s (%2$s) VALUES (%3$s);',
                    $this->tableName,
                    $this->getSqlFields(),
                    $this->getSqlValues($record)
                );
            }

            return $sqlInserts;
        } catch (\Exception | \Error $e) {
            return [];
        }
    }

    /**
     * @return \PhpOffice\PhpSpreadsheet\Spreadsheet
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    protected function getSpreadsheet(): PS\Spreadsheet
    {
        if ($this->spreadsheet instanceof PS\Spreadsheet) {
            return $this->spreadsheet;
        }

        return $this->spreadsheet = PS\IOFactory::load($this->filePath);
    }

    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    protected function getRecords(): array
    {
        $records = [];

        foreach ($this->getSpreadsheet()->getActiveSheet()->getRowIterator() as $row) {
            $recordData = [];

            foreach ($this->fieldNames as $fieldName => $fieldColumn) {
                try {
                    $cellValue = $this->getSpreadsheet()->getActiveSheet()->getCell(
                        $fieldColumn . $row->getRowIndex()
                    )->getValue();
                } catch (\Exception | \Error $e) {
                    $cellValue = null;
                }

                $recordData[$fieldName] = $cellValue;
            }

            foreach ($this->defaults as $defaultFieldName => $defaultValue) {
                if (array_key_exists($defaultFieldName, $recordData) !== true
                    || $recordData[$defaultFieldName] === null) {
                    $recordData[$defaultFieldName] = $defaultValue;
                }
            }

            $records[] = $recordData;
        }

        return $records;
    }

    /**
     * @return string
     */
    protected function getSqlFields(): string
    {
        return implode(
            ', ',
            array_map(
                function (string $fieldName) {
                    return sprintf('`%s`', $fieldName);
                },
                array_keys(array_merge($this->fieldNames, $this->defaults))
            )
        );
    }

    /**
     * @param $record
     *
     * @return string
     */
    protected function getSqlValues($record): string
    {
        return implode(
            ', ',
            array_map(
                function (?string $value = null) {
                    return sprintf(
                        '"%s"',
                        strtr($value, ['"' => '\"'])
                    );
                },
                $record
            )
        );
    }

}