<?php

namespace GetNoticed\Base64\Commands;

use Symfony\Component\Console\{Command\Command,
    Input\InputArgument,
    Input\InputDefinition,
    Input\InputInterface,
    Output\OutputInterface};

class Encode extends Command
{
    protected static $defaultName = 'base64:encode';

    protected function configure()
    {
        $this->setDescription('Encodes the given content using base64.');
        $this->setDefinition(
            new InputDefinition(
                [
                    new InputArgument('content', InputArgument::REQUIRED, 'The content to encode using base64.')
                ]
            )
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $content = $input->getArgument('content');
        $output->writeln(\base64_encode($content));
    }
}
