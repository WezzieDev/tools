<?php

namespace GetNoticed\Base64\Commands;

use Symfony\Component\Console\{Command\Command,
    Input\InputArgument,
    Input\InputDefinition,
    Input\InputInterface,
    Output\OutputInterface};

class Decode extends Command
{
    protected static $defaultName = 'base64:decode';

    protected function configure()
    {
        $this->setDescription('Decodes the given content using base64.');
        $this->setDefinition(
            new InputDefinition(
                [
                    new InputArgument('content', InputArgument::REQUIRED, 'The content to decode using base64.')
                ]
            )
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $content = $input->getArgument('content');
        $output->writeln(\base64_decode($content));
    }
}
