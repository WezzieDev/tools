<?php

namespace GetNoticed\Magento\Commands\Generate;

use Symfony\Component\Console;

class GenerateCronRules extends Console\Command\Command
{
    const TEMPLATES = [
        '* * * * * <path to php binary> <magento install dir>/bin/magento cron:run | grep -v "Ran jobs by schedule" >> <magento install dir>/var/log/magento.cron.log',
        '* * * * * <path to php binary> <magento install dir>/update/cron.php >> <magento install dir>/var/log/update.cron.log',
        '* * * * * <path to php binary> <magento install dir>/bin/magento setup:cron:run >> <magento install dir>/var/log/setup.cron.log'
    ];

    protected static $defaultName = 'magento:generate:cron-rules';

    const CMD_DESCRIPTION = 'This command will generate the necessary cron rules to setup on your server.';
    const CMD_HELP = 'Run the command and follow the interactive steps to generate the cron rules.';

    const ARG_MAGENTO_ROOT = 'magento-root';
    const ARG_PHP_PATH = 'php-path';

    protected function configure()
    {
        $this->setDescription(self::CMD_DESCRIPTION)->setHelp(self::CMD_HELP);
        $this->setDefinition(
            new Console\Input\InputDefinition(
                [
                    new Console\Input\InputArgument(
                        self::ARG_PHP_PATH,
                        Console\Input\InputArgument::OPTIONAL,
                        'Specify the path to the PHP binary',
                        '/usr/local/bin/php'
                    ),
                    new Console\Input\InputArgument(
                        self::ARG_MAGENTO_ROOT,
                        Console\Input\InputArgument::OPTIONAL,
                        'Specify the path to the Magento root',
                        '~/deploy/current'
                    )
                ]
            )
        );
    }

    protected function execute(Console\Input\InputInterface $input, Console\Output\OutputInterface $output)
    {
        foreach (self::TEMPLATES as $template) {
            $output->writeln(
                strtr(
                    $template,
                    [
                        '<path to php binary>'  => $input->getArgument(self::ARG_PHP_PATH),
                        '<magento install dir>' => $input->getArgument(self::ARG_MAGENTO_ROOT)
                    ]
                )
            );
        }
    }
}