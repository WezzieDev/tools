<?php

namespace GetNoticed\Magento\Commands\Generate;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use League\Csv\Reader;

class EavSelectOffline extends Command
{
    protected static $defaultName = 'magento:generate:eav-select:offline';

    protected function configure()
    {
        $this
            ->setDescription('Generate an SQL query to load EAV-attribute data.')
            ->setHelp('This command allows you to generate an SQL query with which EAV-attribute data can be loaded.');

        $this
            ->addOption(
                'entity_type',
                't',
                InputOption::VALUE_REQUIRED,
                'Define the entity type you wish to use (e.g. catalog_category)'
            )
            ->addOption(
                'eav_attributes',
                'a',
                InputOption::VALUE_OPTIONAL,
                'Define additional EAV attributes to load.'
            )
            ->addOption(
                'store_id',
                's',
                InputOption::VALUE_OPTIONAL,
                'Select attributes from a specific store view, rather than the global scope.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entityType = $input->getOption('entity_type');
        $eavAttributes = $this->processRawEavAttributes($input->getOption('eav_attributes'));

        if (is_null($entityType) || strlen($entityType) < 1) {
            throw new \Exception('Invalid entity type');
        }

        $baseTable = sprintf('%s_entity', $entityType);
        $baseTableAlias = 'main';
        $select = ['main.*'];
        $joins = [];
        $storeId = (int)$input->getOption('store_id') ?? 0;

        foreach ($eavAttributes as $attribute) {
            list($attributeId, $attributeCode, $backendType) = $attribute;
            $backendTable = sprintf('%s_%s', $baseTable, $backendType);
            $tableAlias = sprintf('at_%s', $attributeId);
            $scopedTableAlias = sprintf('ats_%s', $attributeId);

            if ($storeId === 0) {
                $joins[] = sprintf(
                    'LEFT JOIN %1$s %2$s ON %2$s.attribute_id = %3$s AND %2$s.store_id = 0 AND %2$s.entity_id = %4$s.entity_id',
                    $backendTable,
                    $tableAlias,
                    $attributeId,
                    $baseTableAlias
                );
                $select[] = sprintf('%s.value AS %s', $tableAlias, $attributeCode);
            } else {
                $joins[] = sprintf(
                    'LEFT JOIN %1$s %2$s ON %2$s.attribute_id = %3$s AND %2$s.store_id = 0 AND %2$s.entity_id = %4$s.entity_id',
                    $backendTable,
                    $tableAlias,
                    $attributeId,
                    $baseTableAlias
                );
                $joins[] = sprintf(
                    'LEFT JOIN %1$s %2$s ON %2$s.attribute_id = %3$s AND %2$s.store_id = %5$s AND %2$s.entity_id = %4$s.entity_id',
                    $backendTable,
                    $scopedTableAlias,
                    $attributeId,
                    $baseTableAlias,
                    $storeId
                );
                $select[] = sprintf(
                    'IF(%2$s.value_id IS NULL, %1$s.value, %2$s.value) AS %3$s', $tableAlias, $scopedTableAlias,
                    $attributeCode
                );
            }
        }

        $query = sprintf(
            '
            SELECT %s
            FROM %s %s
            %s',
            implode(', ', $select), $baseTable, $baseTableAlias, implode(PHP_EOL, $joins)
        );

        $output->writeln($query);
    }

    protected function processRawEavAttributes($rawInput)
    {
        $eavAttributes = [];

        $csv = Reader::createFromString($rawInput);
        $csv->setDelimiter('	');

        foreach ($csv as $record) {
            list($attributeId, $attributeCode, $backendType) = $record;

            if ('backend_type' != $backendType) {
                $eavAttributes[] = $record;
            }
        }

        return $eavAttributes;
    }

}