<?php declare(strict_types=1);

namespace GetNoticed\Magento\Commands\Generate;

use Symfony\Component\Console;

class GenerateEnvFileCommand
    extends Console\Command\Command
{

    protected static $defaultName = 'magento:generate:env-file';

    const CMD_DESCRIPTION = 'This interactive command helps you to generate an env.php file for an initial server setup.';
    const CMD_HELP = 'Run the command and follow the interactive steps to generate an env.php file';

    const ARG_OUTPUT_FILE_PATH = 'output-file-path';

    protected function configure()
    {
        $this->setDescription(self::CMD_DESCRIPTION)->setHelp(self::CMD_HELP);
        $this->setDefinition($this->createDefinition());
    }

    protected function execute(Console\Input\InputInterface $input, Console\Output\OutputInterface $output)
    {
        // Create IO object
        $io = new Console\Style\SymfonyStyle($input, $output);

        // Variables used in config (user will be prompted for each)
        $adminFrontName = $io->ask('Enter the path of the admin panel', 'backoffice');
        $cryptKey = $io->ask(
            'Enter the crypt key to use (if left empty, using generated crypt key)',
            $this->generateCryptKey()
        );
        $redisServer = $io->ask('Enter the redis server IP/hostname', '127.0.0.1');
        $redisPort = $io->ask('Enter the redis port', '6379');
        $tablePrefix = $io->ask(
            'Enter the table prefix (NOTE: we recommend leaving the table prefix empty at all times', ''
        );
        $dbHost = $io->ask('Enter your database host', 'localhost');
        $dbName = $io->ask('Enter the name of your database', '');
        $dbUser = $io->ask('Enter the database username', '');
        $dbPassword = $io->ask('Enter your database password', '');
        $installationDate = $this->getInstallationDate();
        $caches = [
            'config'                 => 1,
            'layout'                 => 1,
            'block_html'             => 1,
            'collections'            => 1,
            'reflection'             => 1,
            'db_ddl'                 => 1,
            'eav'                    => 1,
            'customer_notification'  => 1,
            'config_integration'     => 1,
            'config_integration_api' => 1,
            'full_page'              => 1,
            'translate'              => 1,
            'config_webservice'      => 1
        ];

        // Create the $env variable so we can generate the file for the user.
        $env = [
            'backend'         => [
                'frontName' => $adminFrontName
            ],
            'crypt'           => [
                'key' => $cryptKey
            ],
            'cache'           => [
                'frontend' => [
                    'default'    => [
                        'backend'         => 'Cm_Cache_Backend_Redis',
                        'backend_options' => [
                            'server' => $redisServer,
                            'port'   => $redisPort
                        ]
                    ],
                    'page_cache' => [
                        'backend'         => 'Cm_Cache_Backend_Redis',
                        'backend_options' => [
                            'server'        => $redisServer,
                            'port'          => $redisPort,
                            'database'      => '1',
                            'compress_data' => '0'
                        ]
                    ]
                ]
            ],
            'session'         => [
                'save'  => 'redis',
                'redis' => [
                    'host'                  => $redisServer,
                    'port'                  => $redisPort,
                    'password'              => '',
                    'timeout'               => '2.5',
                    'persistent_identifier' => '',
                    'database'              => '0',
                    'compression_threshold' => '2048',
                    'compression_library'   => 'gzip',
                    'log_level'             => '1',
                    'max_concurrency'       => '6',
                    'break_after_frontend'  => '5',
                    'break_after_adminhtml' => '30',
                    'first_lifetime'        => '600',
                    'bot_first_lifetime'    => '60',
                    'bot_lifetime'          => '7200',
                    'disable_locking'       => '0',
                    'min_lifetime'          => '60',
                    'max_lifetime'          => '2592000'
                ]
            ],
            'db'              => [
                'table_prefix' => $tablePrefix,
                'connection'   => [
                    'default' => [
                        'host'     => $dbHost,
                        'dbname'   => $dbName,
                        'username' => $dbUser,
                        'password' => $dbPassword,
                        'active'   => '1'
                    ]
                ]
            ],
            'resource'        => [
                'default_setup' => [
                    'connection' => 'default'
                ]
            ],
            'x-frame-options' => 'SAMEORIGIN',
            'MAGE_MODE'       => 'production',
            'cache_types'     => $caches,
            'install'         => [
                'date' => $installationDate
            ]
        ];

        \file_put_contents($this->getEnvFilePath($input), $this->generateEnvFile($env));

        $io->success(sprintf('Written file to path: %s', $this->getEnvFilePath($input)));
    }

    protected function createDefinition(): Console\Input\InputDefinition
    {
        return new Console\Input\InputDefinition(
            [
                new Console\Input\InputArgument(
                    self::ARG_OUTPUT_FILE_PATH,
                    Console\Input\InputArgument::REQUIRED,
                    'Where should the resulting env.php file be written to?'
                )
            ]
        );
    }

    private function generateCryptKey()
    {
        return $this->generateRandomString(
            32,
            array_merge(
                range('a', 'z'),
                range('A', 'Z'),
                range(0, 9)
            )
        );
    }

    private function generateRandomString(int $length, array $chars)
    {
        if ($length < 1 || $length > 500) {
            throw new \InvalidArgumentException('Invalid length: must be between 1 and 500.');
        }

        if (count($chars) < 1) {
            throw new \InvalidArgumentException('Please specify a valid list of characters.');
        }

        $output = [];

        for ($i = 0; $i < $length; $i++) {
            $output[] = $this->getRandomCharacterFromArray($chars);
        }

        return implode('', $output);
    }

    /**
     * @return string
     */
    protected function getInstallationDate(): string
    {
        return (new \DateTime())->format('r');
    }

    /**
     * @param array $chars
     *
     * @return int
     */
    private function getRandomIndex(array $chars): int
    {
        try {
            $index = random_int(0, count($chars) - 1);
        } catch (\Exception $e) {
            $index = rand(0, count($chars) - 1);
        }

        return $index;
    }

    /**
     * @param array $chars
     *
     * @return mixed
     */
    private function getRandomCharacterFromArray(array $chars)
    {
        return $chars[$this->getRandomIndex($chars)];
    }

    /**
     * @param $env
     *
     * @return string
     */
    protected function generateEnvFile($env): string
    {
        return sprintf("<?php\n\nreturn %s;", var_export($env, true));
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     *
     * @return null|string|string[]
     */
    protected function getEnvFilePath(Console\Input\InputInterface $input)
    {
        return $input->getArgument(self::ARG_OUTPUT_FILE_PATH);
    }

}