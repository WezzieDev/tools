<?php

namespace GetNoticed\Magento\Commands\Generate;

use Symfony\Component\Console;

class EavSelectOnline extends Console\Command\Command
{
    protected static $defaultName = 'magento:generate:eav-select:online';

    /**
     * @var \PDO
     */
    private $dbConnection;

    /**
     * @var string
     */
    private $dbHost = 'localhost';

    /**
     * @var string
     */
    private $dbUsername = 'root';

    /**
     * @var string
     */
    private $dbPassword = 'root';

    /**
     * @var string
     */
    private $dbName;

    /**
     * @var int
     */
    private $dbPort = 3306;

    /**
     * @var array
     */
    private $entityTypes = [];

    protected function configure()
    {
        $this
            ->setDescription('Load EAV data from the selected project and display it in a table.')
            ->setHelp(
                'Run this command to display EAV data. Configuration can be pre-specified through options and arguments for easier usage.'
            );

        $this->setDefinition(
            new Console\Input\InputDefinition(
                [
                    new Console\Input\InputOption(
                        'project',
                        'p',
                        Console\Input\InputOption::VALUE_OPTIONAL,
                        'Project to use',
                        null
                    ),
                    new Console\Input\InputOption(
                        'entity',
                        't',
                        Console\Input\InputOption::VALUE_OPTIONAL,
                        'Entity type to use',
                        null
                    ),
                    new Console\Input\InputOption(
                        'fields',
                        'f',
                        Console\Input\InputOption::VALUE_OPTIONAL | Console\Input\InputOption::VALUE_IS_ARRAY,
                        'Fields to load in the select',
                        []
                    ),
                    new Console\Input\InputOption(
                        'where',
                        'w',
                        Console\Input\InputOption::VALUE_OPTIONAL | Console\Input\InputOption::VALUE_IS_ARRAY,
                        'WHERE statements to perform'
                    ),
                    new Console\Input\InputOption(
                        'having',
                        's',
                        Console\Input\InputOption::VALUE_OPTIONAL | Console\Input\InputOption::VALUE_IS_ARRAY,
                        'HAVING statements to perform'
                    ),
                    new Console\Input\InputOption(
                        'db-host',
                        null,
                        Console\Input\InputOption::VALUE_OPTIONAL,
                        'The database host to connect to.'
                    ),
                    new Console\Input\InputOption(
                        'db-user',
                        null,
                        Console\Input\InputOption::VALUE_OPTIONAL,
                        'The database user to connect as.'
                    ),
                    new Console\Input\InputOption(
                        'db-password',
                        null,
                        Console\Input\InputOption::VALUE_OPTIONAL,
                        'The database password to connect with.'
                    ),
                    new Console\Input\InputOption(
                        'db-name',
                        null,
                        Console\Input\InputOption::VALUE_OPTIONAL,
                        'The database name to connect with.'
                    ),
                    new Console\Input\InputOption(
                        'db-port',
                        null,
                        Console\Input\InputOption::VALUE_OPTIONAL,
                        'The database port to use.'
                    ),
                    new Console\Input\InputOption(
                        'debug',
                        null,
                        Console\Input\InputOption::VALUE_OPTIONAL,
                        false
                    )
                ]
            )
        );
    }

    protected function execute(Console\input\InputInterface $input, Console\Output\OutputInterface $output)
    {
        $io = new Console\Style\SymfonyStyle($input, $output);
        $isDebug = $input->getOption('debug');

        if ($input->getOption('db-host')) {
            $this->dbHost = $input->getOption('db-host');
            $this->dbUsername = $input->getOption('db-user');
            $this->dbPassword = $io->askHidden('Enter the database password');
            $this->dbName = $input->getOption('db-name');
            $this->dbPort = $input->getOption('db-port');
        } else {
            // Get config from project
            $project = $input->getOption('project') ?: $io->askQuestion(
                new Console\Question\ChoiceQuestion(
                    'Please select your project',
                    $this->getAvailableProjects()
                )
            );

            // Get env config + load DB config
            $envConfig = $this->getEnvConfig($project);
            $this->loadDbConfig($envConfig);
        }

        $entityTypes = $this->getAvailableEntities();
        $entity = $input->getOption('entity') ?: $io->askQuestion(
            new Console\Question\ChoiceQuestion('Please select your entity', $entityTypes)
        );
        $entity = $this->entityTypes[$entity];
        $attributes = $this->getAttributes($entity);
        $staticFields = $this->getStaticFields(
            $entity['entity_table'],
            array_map(
                function (array $attribute) {
                    return $attribute['attribute_code'];
                },
                $attributes['static']
            )
        );
        $eavFields = $this->getEavFields($attributes['eav']);
        $allFields = array_values(array_merge([0 => 'Done'], $staticFields, $eavFields));

        $selectData = $input->getOption('fields') ?: [];

        if (empty($selectData)) {
            do {
                $attributeCode = $io->askQuestion(
                    new Console\Question\ChoiceQuestion(
                        'Select (an)other attribute to select',
                        $allFields
                    )
                );

                if (empty($attributeCode) || $attributeCode === 'Done') {
                    break;
                }

                if ($attributeCode !== null) {
                    $selectData[] = $attributeCode;

                    $idx = array_search($attributeCode, $allFields);
                    unset($allFields[$idx]);
                }
            } while ($attributeCode !== null);
        }

        $select = $joins = $headers = [];

        foreach ($selectData as $attributeCode) {
            $headers[] = $attributeCode;
            $attributeData = $this->getAttributeData($attributeCode, $attributes);

            if (is_array($attributeData)) {
                if ($attributeData['backend_type'] === 'static') {
                    $select[] = sprintf('main.%s', $attributeData['attribute_code']);
                } else {
                    $joins[] = sprintf(
                        'LEFT JOIN %1$s %2$s ON %2$s.attribute_id = %3$s AND %2$s.entity_id = %4$s.entity_id',
                        $attributeData['backend_table']
                            ?: sprintf('%s_%s', $entity['entity_table'], $attributeData['backend_type']),
                        sprintf('attribute_%d', $attributeData['attribute_id']),
                        $attributeData['attribute_id'],
                        'main'
                    );
                    $select[] = sprintf(
                        '%s.value AS %s',
                        sprintf('attribute_%d', $attributeData['attribute_id']),
                        $attributeData['attribute_code']
                    );
                }
            } else {
                // Just a static non-attribute field
                $select[] = sprintf('main.%s', $attributeData);
            }
        }

        $where = $input->getOption('where') ?: [];
        $having = $input->getOption('having') ?: [];

        $generatedSql = sprintf(
            'SELECT %s FROM %s %s %s %s %s',
            implode(', ', $select),
            $entity['entity_table'],
            'main',
            implode(PHP_EOL, $joins),
            !empty($where) ? 'WHERE ' . implode(' AND ', $where) : '',
            !empty($having) ? 'HAVING ' . implode(' AND ', $having) : ''
        );
        $query = $this->dbStatement($generatedSql);

        if ($isDebug) {
            $io->writeln(sprintf('<info>DEBUG:query></info> %s', $generatedSql));
        }

        $io->table($headers, $query->fetchAll(\PDO::FETCH_NUM));
    }

    private function getAvailableProjects(): array
    {
        $dir = $this->getProjectsDirectory();

        if (is_dir($dir) !== true) {
            throw new \RuntimeException('Invalid projects directory!');
        }

        $projects = [];
        $iterator = new \DirectoryIterator($dir);

        foreach ($iterator as $file) {
            if (in_array($file->getFilename(), ['.', '..']) || !is_dir($file->getRealPath())) {
                continue;
            }

            if (file_exists($file->getRealPath() . '/app/etc/env.php') !== true) {
                continue;
            }

            $projects[] = $file->getFilename();
        }

        return $projects;
    }

    /**
     * @return bool|string
     */
    private function getProjectsDirectory()
    {
        return realpath(getenv('PROJECTS_DIRECTORY'));
    }

    /**
     * @param $envConfig
     */
    protected function loadDbConfig($envConfig): void
    {
        $dbConfig = $envConfig['db']['connection']['default'];
        $this->dbHost = $dbConfig['host'];
        $this->dbUsername = $dbConfig['username'];
        $this->dbPassword = $dbConfig['password'];
        $this->dbName = $dbConfig['dbname'];
        $this->dbPort = array_key_exists('port', $dbConfig) ? $dbConfig['port'] : 3306;
    }

    /**
     * @param $project
     *
     * @return mixed
     */
    protected function getEnvConfig($project)
    {
        $envPath = $this->getProjectsDirectory() . DS . $project . DS . 'app' . DS . 'etc' . DS . 'env.php';
        $envConfig = require $envPath;

        return $envConfig;
    }

    private function getAvailableEntities(): array
    {
        $res = $this->dbStatement('SELECT * FROM eav_entity_type');
        $entities = [];

        foreach ($res->fetchAll(\PDO::FETCH_ASSOC) as $entity) {
            $this->entityTypes[$entity['entity_type_code']] = $entity;
            $entities[] = $entity['entity_type_code'];
        }

        return $entities;
    }

    private function getDbConnection()
    {
        if ($this->dbConnection instanceof \PDO) {
            return $this->dbConnection;
        }

        return $this->dbConnection = new \PDO(
            sprintf('mysql:dbname=%s; host=%s; port=%d', $this->dbName, $this->dbHost, $this->dbPort),
            $this->dbUsername,
            $this->dbPassword
        );
    }

    /**
     * @param string $sql
     * @param array  $args
     *
     * @return \PDOStatement
     */
    private function dbStatement(string $sql, array $args = []): \PDOStatement
    {
        $stmt = $this->getDbConnection()->prepare($sql);
        $stmt->execute($args);

        return $stmt;
    }

    private function getAttributes(array $entity): array
    {
        $attributesSelect = $this->dbStatement(
            'SELECT * FROM eav_attribute WHERE entity_type_id = ?',
            [$entity['entity_type_id']]
        );

        $attributes = [
            'static' => [],
            'eav'    => []
        ];

        foreach ($attributesSelect->fetchAll(\PDO::FETCH_ASSOC) as $attribute) {
            if ($attribute['backend_type'] === 'static') {
                $attributes['static'][] = $attribute;
            } else {
                $attributes['eav'][] = $attribute;
            }
        }

        return $attributes;
    }

    private function getStaticFields(string $tableName, array $extraFields = []): array
    {
        $describeSelect = $this->dbStatement('DESCRIBE ' . $tableName);

        $fields = [];

        foreach ($extraFields as $extraField) {
            $fields[$extraField] = $extraField;
        }

        foreach ($describeSelect->fetchAll(\PDO::FETCH_ASSOC) as $field) {
            $fields[$field['Field']] = $field['Field'];
        }

        return $fields;
    }

    /**
     * @param array $attributes
     *
     * @return array
     */
    protected function getEavFields(array $attributes): array
    {
        $fields = [];

        foreach ($attributes as $attribute) {
            $fields[$attribute['attribute_code']] = $attribute['attribute_code'];
        }

        return $fields;
    }

    protected function getAttributeData(string $code, array $attributes)
    {
        $static = $attributes['static'];
        $eav = $attributes['eav'];

        foreach ($static as $attribute) {
            if ($attribute['attribute_code'] === $code) {
                return $attribute;
            }
        }

        foreach ($eav as $attribute) {
            if ($attribute['attribute_code'] === $code) {
                return $attribute;
            }
        }

        return $code;
    }
}