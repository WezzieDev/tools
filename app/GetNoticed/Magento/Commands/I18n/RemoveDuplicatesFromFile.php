<?php

namespace GetNoticed\Magento\Commands\I18n;

use Symfony\Component\Console;
use League\Csv;

class RemoveDuplicatesFromFile
    extends Console\Command\Command
{

    const ARG_CSV_FILE = 'csv-file';

    protected function configure()
    {
        $this
            ->setName('magento:i18n:remove-duplicates-from-file')
            ->setDescription('If given a CSV as argument, it will remove all and any duplicate lines.')
            ->setHelp('If given a CSV as argument, it will remove all and any duplicate lines.');

        $this->addArgument(
            self::ARG_CSV_FILE,
            Console\Input\InputArgument::REQUIRED,
            'The path to the CSV file'
        );
    }

    protected function execute(
        Console\Input\InputInterface $input,
        Console\Output\OutputInterface $output
    ) {
        // Validate file path
        $csvPath = $input->getArgument(self::ARG_CSV_FILE);

        if (is_file($csvPath) !== true) {
            $output->writeln(sprintf('<error>%s</error>', 'Error: not a valid file'));
        }

        $writer = Csv\Writer::createFromPath($csvPath, 'w+');
        $writer->setDelimiter(',')->setEnclosure('"');
        $writer->insertAll($this->getTranslations($csvPath));

        $output->writeln('<info>Successfully rewrote CSV file!</info>');
    }

    /**
     * @param $csv
     *
     * @return array
     */
    protected function getTranslationsFromCsv(Csv\Reader $csv): array
    {
        $translations = [];

        foreach ($csv as $record) {
            $original = current($record);

            if (array_key_exists($original, $translations) === true) {
                // Skip, don't include twice
                continue;
            }

            $translations[$original] = $record;
        }

        return $translations;
    }

    /**
     * @param $csvPath
     *
     * @return array
     */
    protected function getTranslations($csvPath): array
    {
        $csv = Csv\Reader::createFromPath($csvPath, 'r');
        $csv->setOffset(0);

        // Flatten the array and make it unique, then write it back to CSV file
        return $this->getTranslationsFromCsv($csv);
    }

}