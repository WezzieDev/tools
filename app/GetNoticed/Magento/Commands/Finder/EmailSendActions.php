<?php

namespace GetNoticed\Magento\Commands\Finder;

use GetNoticed\Utils\FileUtils;
use Symfony\Component\Console\{Command\Command,
    Input\InputDefinition,
    Input\InputInterface,
    Input\InputOption,
    Output\OutputInterface,
    Style\SymfonyStyle};

class EmailSendActions extends Command
{
    protected static $defaultName = 'magento:finder:email-send-actions';

    /**
     * @var string[]
     */
    private $defaultTransportBuilderClasses = [
        'Magento\Framework\Mail\Template\TransportBuilder',
        'Magento\Framework\Mail\Template\TransportBuilderByStore'
    ];

    /**
     * @var string[]
     */
    private $transportBuilderClasses = [];

    /**
     * @var string[]
     */
    private $emailTemplatesConfigFiles = [];

    /**
     * @var array[]
     */
    private $emailTemplates = [];

    /**
     * @var string
     */
    private $magentoRoot;

    protected function configure()
    {
        $this
            ->setDescription('Finds all sections in Magento that send an e-mail and displays them in a list.')
            ->setHelp('Run this command to find all e-mails sent by Magento.');

        $this->setDefinition(
            new InputDefinition(
                [
                    new InputOption(
                        'transport-builder-classes',
                        'c',
                        InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY,
                        'All classes that function as a transport builder.', []
                    ),
                    new InputOption(
                        'magento-root',
                        'r',
                        InputOption::VALUE_REQUIRED,
                        'The Magento installation root path.'
                    )
                ]
            )
        );
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Input args / configuration
        $this->initializeArgs($input);
        $this->emailTemplatesConfigFiles = array_merge(
            FileUtils::findWholename(
                sprintf('%s/app/*/etc/email_templates.xml', $this->magentoRoot), $this->magentoRoot
            ),
            FileUtils::findWholename(
                sprintf('%s/vendor/*/etc/email_templates.xml', $this->magentoRoot), $this->magentoRoot
            )
        );
        $this->emailTemplates = $this->loadEmailTemplates();
        $this->parseEmailUsages();

        // Output usefull data
        $io = new SymfonyStyle($input, $output);

        foreach ($this->emailTemplates as $emailTemplate) {
            $io->title(sprintf('Template: %s (%s)', $emailTemplate['label'], $emailTemplate['id']));
            $io->table(
                [
                    'File location',
                    'Object location'
                ],
                array_map(
                    function (array $usageRow) {
                        return [
                            sprintf('%s:%s', $usageRow['file'], $usageRow['line']),
                            sprintf('%s::%s', $usageRow['class'], $usageRow['method'])
                        ];
                    },
                    $emailTemplate['usages']
                )
            );
        }

        return 0;
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     *
     * @throws \Exception
     */
    protected function initializeArgs(InputInterface $input): void
    {
        $this->transportBuilderClasses = array_merge(
            $this->defaultTransportBuilderClasses, $input->getOption('transport-builder-classes')
        );
        $this->magentoRoot = $input->getOption('magento-root');

        if (empty($this->magentoRoot)) {
            throw new \Exception('"magento-root" is a required option.');
        }
    }

    private function loadEmailTemplates(): array
    {
        $emails = [];

        foreach ($this->emailTemplatesConfigFiles as $emailTemplatesConfigFile) {
            $config = new \SimpleXMLElement(\file_get_contents($emailTemplatesConfigFile));

            foreach ($config->template as $template) {
                $templateAttributes = $template->attributes();
                $emailId = (string)$templateAttributes['id'];
                $label = (string)$templateAttributes['label'];
                $file = (string)$templateAttributes['file'];
                $type = (string)$templateAttributes['type'];
                $module = (string)$templateAttributes['module'];
                $area = (string)$templateAttributes['area'];

                $emails[] = [
                    'id'     => $emailId,
                    'label'  => $label,
                    'file'   => $file,
                    'type'   => $type,
                    'module' => $module,
                    'area'   => $area,
                    'usages' => []
                ];
            }
        }

        return $emails;
    }

    private function parseEmailUsages()
    {
        /*$tb->getTransport()->sendMessage()*/
        foreach ($this->emailTemplates as &$email) {
            $findCommands = [
                sprintf(
                    'find %s -type f -name "*.php" -exec grep -Hn -B100 \'%s\' {} \;', $this->magentoRoot . '/app/',
                    $email['id']
                ),
                sprintf(
                    'find %s -type f -name "*.php" -exec grep -Hn -B100 \'%s\' {} \;', $this->magentoRoot . '/vendor/',
                    $email['id']
                )
            ];

            foreach ($findCommands as $findCommand) {
                exec($findCommand, $output, $exitCode);

                foreach (array_reverse($output) as $line) {
                    $results = preg_match(
                        sprintf(
                            '#(%s)(?P<file>.*)(\-)(?P<line>.*)(\-)(.*)(public|protected|private)\ function\ (?P<method>.*)\(#',
                            $this->magentoRoot
                        ),
                        $line,
                        $matches
                    );

                    if ($results > 0) {
                        $email['usages'][] = [
                            'file'   => $matches['file'],
                            'line'   => $matches['line'],
                            'method' => $matches['method'],
                            'class'  => $this->getClassNameFromFile($matches['file'])
                        ];

                        break;
                    }
                }
            }
        }
    }

    private function getClassNameFromFile(string $filePath): ?string
    {
        $fullPath = $this->magentoRoot . DS . $filePath;
        $contents = \file_get_contents($fullPath);

        preg_match('#(namespace\ )(?P<namespace>.*)\;(.*)#', $contents, $namespaceMatch);
        preg_match('#(class\ )(?P<class>\w*)#', $contents, $classMatch);

        if (!empty($namespaceMatch) && !empty($namespaceMatch['namespace'])
            && !empty($classMatch) && !empty($classMatch['class'])) {
            return sprintf('%s\\%s', $namespaceMatch['namespace'], $classMatch['class']);
        }

        return null;
    }
}
