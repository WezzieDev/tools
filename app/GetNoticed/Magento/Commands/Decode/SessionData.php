<?php

namespace GetNoticed\Magento\Commands\Decode;

use Symfony\Component\Console;

class SessionData extends Console\Command\Command
{

    const INPUT_ARG_SESSION_DATA = 'data';

    const INPUT_OPTION_IS_FILE = 'is-file';

    protected function configure()
    {
        $this
            ->setName('magento:decode:session-data')
            ->setDescription('Decodes the session data objects and displays them for easier reading.')
            ->setHelp(
                'This command allows you to decode session data and will display it pretty-printed for easier reading.'
            );

        $this->addArgument(
            self::INPUT_ARG_SESSION_DATA, Console\Input\InputArgument::REQUIRED, 'The encoded Magento session data'
        );
        $this->addOption(
            self::INPUT_OPTION_IS_FILE,
            null,
            Console\Input\InputOption::VALUE_OPTIONAL,
            'Is the session data argument a path to file.',
            false
        );
    }

    protected function execute(Console\Input\InputInterface $input, Console\Output\OutputInterface $output)
    {
        session_start();

        $encoded = $input->getArgument(self::INPUT_ARG_SESSION_DATA);
        $isFile = (bool)$input->getOption(self::INPUT_OPTION_IS_FILE);
        $io = new Console\Style\SymfonyStyle($input, $output);

        if ($isFile) {
            $io->note('Decoding from file');        }

        if (session_decode($this->getSessionData($encoded, $isFile))) {
            $io->success('OK:');
            $io->text(var_export($_SESSION, true));
        } else {
            $io->error('Failed to decode session data.');
        }
    }

    protected function getSessionData(string $encoded, bool $isFile)
    {
        if ($isFile) {
            return \base64_decode(\file_get_contents($encoded));
        } else {
            return \base64_decode($encoded);
        }
    }

}