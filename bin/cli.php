<?php

use Symfony\Component\Console\Application;

// Include Composer autoload
define('DS', DIRECTORY_SEPARATOR);
define('APPLICATION_ROOT', realpath(__DIR__ . DS . '..'));

require_once APPLICATION_ROOT . DS . 'vendor' . DS . 'autoload.php';

// Initialize console
$application = new Application();
$dotenv = new \Symfony\Component\Dotenv\Dotenv();
$dotenv->load(APPLICATION_ROOT . DS . '.env');

// Add commands
$application->addCommands(
    [
        # Data Formatters
        new \GetNoticed\DataFormatter\Commands\ConvertToSql(),

        # Magento
        new \GetNoticed\Magento\Commands\Generate\GenerateEnvFileCommand(),
        new \GetNoticed\Magento\Commands\Generate\GenerateCronRules(),
        new \GetNoticed\Magento\Commands\Generate\EavSelectOnline(),
        new \GetNoticed\Magento\Commands\Generate\EavSelectOffline(),
        new \GetNoticed\Magento\Commands\Decode\SessionData(),
        new \GetNoticed\Magento\Commands\I18n\RemoveDuplicatesFromFile(),
        new \GetNoticed\Magento\Commands\Finder\EmailSendActions(),

        # Aliases
        new \GetNoticed\Aliases\Commands\Generate\MediaWikiFormat(),

        # Base64
        new \GetNoticed\Base64\Commands\Encode(),
        new \GetNoticed\Base64\Commands\Decode(),
    ]
);

// Run application
try {
    $application->run();
} catch (\Exception | \Error $e) {
    echo 'An error occurred during runtime: ' . $e->getMessage(), PHP_EOL;
}